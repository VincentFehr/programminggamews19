﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace programmingGame
{

    public class WhipPull : MonoBehaviour
    {

        private GameObject hookPositions;

        [SerializeField]
        private BoxCollider2D hookCollider;

        [SerializeField]
        private string hookPoint;

        [SerializeField]
        private string pullPoint;

        [SerializeField]
        private string damagePoint;

        [SerializeField]
        private string damagingWhipTag;

        [SerializeField]
        private string pullWhipTag;

        [SerializeField]
        private string hookWhipTag;

        [SerializeField]
        private GameObject firstHatSprite;

        private bool hat1;

        [SerializeField]
        private GameObject secondHatSprite;

        private bool hat2;

        [SerializeField]
        private GameObject thirdHatSprite;

        private bool hat3;

        [SerializeField]
        private TextMeshProUGUI hat;

        [SerializeField]
        private Rigidbody2D rb;

        private bool isHooking;

        public bool IsHooking
        {
            get
            {
                return isHooking;
            }
            set
            {
                isHooking = value;
            }
        }

        private bool enemyHit;

        [SerializeField]
        private bool secondHat;

        
        public bool SecondHat
        {
            get
            {
                return secondHat;
            }
            set
            {
                secondHat = value;
            }
        }

        [SerializeField]
        private bool thirdHat;

        public bool ThirdHat
        {
            get
            {
                return thirdHat;
            }
            set
            {
                thirdHat = value;
            }
        }

        private Vector2 direction;

        [SerializeField]
        private Transform pos;

        private float timer = 0f;

        private float hookSpeedMulti;

        [SerializeField]
        private float travelSpeed;

        bool goAhead = false;

        bool hookReady = true;

        [SerializeField]
        private float moveSpeed = 10f;


        private void OnEnable()
        {
            Message<EventWhipCollision>.Add(DamagingWhipCallback);

            Message<EventHooking>.Add(HookCallback);

            Message<EventWhichHat>.Add(SwitchCallback);

        }

        private void OnDisable()
        {
            Message<EventWhipCollision>.Remove(DamagingWhipCallback);

            Message<EventHooking>.Remove(HookCallback);

            Message<EventWhichHat>.Remove(SwitchCallback);
        }

        private void SwitchCallback(EventWhichHat ev)
        {
            hat1 = ev.Hat1;
            hat2 = ev.Hat2;
            hat3 = ev.Hat3;
        }


        private void HookCallback(EventHooking ev)
        {
            isHooking = ev.IsHooking;
        }
        private void DamagingWhipCallback(EventWhipCollision ev)
        {
            enemyHit = ev.EnemyHit;
        }

        void Whip()
        {
            if (Input.GetKeyDown(KeyCode.Mouse1) && timer == 0)
            {
                goAhead = true;
                gameObject.GetComponent<Collider2D>().enabled = true;
                gameObject.GetComponent<SpriteRenderer>().enabled = true;

            }
            if (goAhead == true)
            {
                timer += Time.deltaTime;
            }
            if (timer >= 0.4f)
            {
                gameObject.GetComponent<Collider2D>().enabled = false;
                gameObject.GetComponent<SpriteRenderer>().enabled = false;
                timer = 0f;
                goAhead = false;
                hookReady = true;
            }
        }
        private void Start()
        {
            hookCollider = GetComponent<BoxCollider2D>();
            gameObject.GetComponent<Collider2D>().enabled = false;
            gameObject.GetComponent<SpriteRenderer>().enabled = false;
            hat.text = "Hook Whip";
            hat1 = true;
        }

        private void Update()
        {
            Whip();
            WhipChange();
        }

        private void WhipChange()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                gameObject.tag = hookWhipTag;
                hat.text = "Hook Whip";
                Message.Raise(new EventWhichHat(true, false, false));
            }
            else if (Input.GetKeyDown(KeyCode.Alpha2) && secondHat == true)
            {
                gameObject.tag = pullWhipTag;
                hat.text = "Pull Whip";
                Message.Raise(new EventWhichHat(false, true, false));
            }
            else if (Input.GetKeyDown(KeyCode.Alpha3) && thirdHat == true)
            {
                gameObject.tag = damagingWhipTag;
                hat.text = "Damage Whip";
                Message.Raise(new EventWhichHat(false, false, true));
            }

            if(hat1 == true)
            {
                firstHatSprite.SetActive(true);
                secondHatSprite.SetActive(false);
                thirdHatSprite.SetActive(false);
            }

            if (hat2 == true)
            {
                firstHatSprite.SetActive(false);
                secondHatSprite.SetActive(true);
                thirdHatSprite.SetActive(false);

            }

            if (hat3 == true)
            {
                firstHatSprite.SetActive(false);
                secondHatSprite.SetActive(false);
                thirdHatSprite.SetActive(true);
            }
        }

        void OnTriggerEnter2D(Collider2D other)
        {

            if (other.tag == pullPoint || other.tag == hookPoint)
            {
                GetComponent<BoxCollider2D>().enabled = false;
            }

            if (other.tag == hookPoint && hookReady == true && gameObject.tag == hookWhipTag)
            {

                StartCoroutine(HookToPoint());
                IEnumerator HookToPoint()
                {

                    Message.Raise(new EventHooking(true));
                    // Getting the difference in position 
                    Vector3 deltaPos = other.gameObject.transform.position - pos.transform.position;
                    // Repeat once per frame for a second:
                    //Makes the speed of the pull the same from every distance
                    hookSpeedMulti = hookCollider.size.magnitude / deltaPos.magnitude;

                    float timePassed = 0;
                    while (timePassed < 1f)
                    {
                        // Increment the time to prevent an infinite loop
                        timePassed += (Time.deltaTime * hookSpeedMulti) * travelSpeed;
                        // Slowly adjust the position and rotation so it approaches the spawn position and rotation
                        pos.transform.position += (deltaPos * Time.deltaTime * hookSpeedMulti) * travelSpeed;
                        // Exit the function (it will begin where it left off the next frame)
                        yield return null;
                    }
                    // Ensures the player is exactly where you want him when the time is up           
                    pos.transform.position = other.gameObject.transform.position;
                    // Waits for a second after the player has returned before running the game again
                    Message.Raise(new EventHooking(false));
                    yield return new WaitForSeconds(1f);



                }

            }

            if (other.tag == pullPoint && hookReady == true && gameObject.tag == pullWhipTag)
            {
                other.GetComponent<Rigidbody2D>();


                StartCoroutine(PullToPoint());
                IEnumerator PullToPoint()
                {

                    Message.Raise(new EventHooking(true));
                    // Getting the difference in position 
                    Vector3 deltaPos = other.gameObject.transform.position - pos.transform.position;
                    // Repeat once per frame for a second:
                    //Makes the speed of the pull the same from every distance
                    hookSpeedMulti = hookCollider.size.magnitude / deltaPos.magnitude;

                    float timePassed = 0;
                    while (timePassed < 1f)
                    {

                        // Increment the time to prevent an infinite loop
                        timePassed += (Time.deltaTime * hookSpeedMulti) * travelSpeed;
                        // Slowly adjust the position and rotation so it approaches the spawn position and rotation
                        other.gameObject.transform.position -= new Vector3(deltaPos.x * Time.deltaTime * hookSpeedMulti * travelSpeed, deltaPos.y * Time.deltaTime * hookSpeedMulti * travelSpeed, 0);
                        // Exit the function (it will begin where it left off the next frame)
                        yield return null;
                    }
                    Message.Raise(new EventHooking(false));
                    // Waits for a second after the player has returned before running the game again
                    yield return new WaitForSeconds(1f);


                }

            }

            if (other.tag == damagePoint && hookReady == true && gameObject.tag == damagingWhipTag)
            {
                Message.Raise(new EventWhipCollision(true));

                if (enemyHit == true)
                {
                    other.gameObject.SetActive(false);
                }
            }

        }
    }
}
