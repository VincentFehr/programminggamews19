﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace programmingGame
{
    public class LevelSelectButton : MonoBehaviour
    {
        [SerializeField]
        private string SceneName;

        private GameObject StartButton;

        private void Start()
        {
            StartButton = GameObject.Find("Start");
        }

        private void OnEnable()
        {
            Message<EventLevelSelect>.Add(EventLevelSelectCallback);
        }

        private void OnDisable()
        {
            Message<EventLevelSelect>.Remove(EventLevelSelectCallback);
        }

        private void EventLevelSelectCallback(EventLevelSelect ev)
        {
        }


        public void SetLevel()
        {
            Message.Raise(new EventLevelSelect(SceneName, StartButton));
        }
    }
}
