﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace programmingGame
{
    public class StartGame : MonoBehaviour
    {
        [SerializeField]
        private string scenename;

        public string Scenename
        {
            get => scenename;
            set => scenename = value;
        }

        public void GameScene()
        {
            SceneManager.LoadScene(scenename);
        }
    }
}
