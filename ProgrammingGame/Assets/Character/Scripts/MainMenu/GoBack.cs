﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoBack : MonoBehaviour
{
    [SerializeField]
    private GameObject instructionsWindow;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void instructions()
    {
        instructionsWindow.gameObject.SetActive(false);
    }
    // Update is called once per frame
    void Update()
    {

    }
}
