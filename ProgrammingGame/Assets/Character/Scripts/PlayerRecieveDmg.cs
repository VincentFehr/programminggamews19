﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

namespace programmingGame
{

    public class PlayerRecieveDmg : MonoBehaviour
    {

        [SerializeField]
        private WhipPull whipPull;
        
        [SerializeField]
        private Tilemap Environment;

        private Vector3Int currentPosition;

        [SerializeField]
        private GameObject[] Swamp;

        [SerializeField]
        private Tile SwampTile;

        private string spawner = "Spawner";

        private Tilemap[] Tilemaps;

        private GameObject[] spawnpoint;
        [SerializeField]
        private GameObject player;

        private void Start()
        {
            spawnpoint = GameObject.FindGameObjectsWithTag(spawner);
            Tilemaps = FindObjectsOfType<Tilemap>();

            for (int i = 0; i < Tilemaps.Length; i++)
            {
              
                if (Tilemaps[i].name == "Environment")
                {
                    Environment = Tilemaps[i];
                }
            }
        }

        private void Update()
        {
            currentPosition.x = Mathf.RoundToInt(transform.position.x);
            currentPosition.y = Mathf.RoundToInt(transform.position.y);

            checkPosition();
        }

        private void OnEnable()
        {
            Message<EventPlayerSpawn>.Add(PlayerSpawnCallback);
        }

        private void OnDisable()
        {
            Message<EventPlayerSpawn>.Remove(PlayerSpawnCallback);
        }

        private void PlayerSpawnCallback(EventPlayerSpawn ev)
        {
            ev.Spawnpoint = spawnpoint[0].transform;
            ev.Player = player;
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Enemy") && gameObject.CompareTag("Player"))
            {
                Debug.Log("hi");
                Message.Raise(new EventPlayerSpawn(player, spawnpoint[0].transform));

                losHealth();
            }
        }

        void checkPosition()
        {
            if (Environment.GetTile(currentPosition) == SwampTile && whipPull.IsHooking == false)
            {
               Message.Raise(new EventPlayerSpawn(player, spawnpoint[0].transform));

               losHealth();
            }
           else
            {
                return;
            }
        }

        void losHealth()
        {
            this.gameObject.GetComponent<RestartLevel>().Currenthealth -= 1;
        }
    }
}