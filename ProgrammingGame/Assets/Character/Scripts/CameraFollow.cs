﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace programmingGame
{

    public class CameraFollow : MonoBehaviour
    {
        [SerializeField]
        private Transform target;

        private Vector3 smoothedPosition;

        [SerializeField]
        private float maxSpeed;

        [SerializeField]
        private float smoothing;

        private Vector3 offset;

        private void LateUpdate()
        {
            Vector3 desiredPosition = target.position;
            Vector3 smoothedPosition = Vector3.SmoothDamp(transform.position, desiredPosition, ref offset, smoothing, maxSpeed);
            transform.position = new Vector3(smoothedPosition.x, smoothedPosition.y, -10);
        }
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}
