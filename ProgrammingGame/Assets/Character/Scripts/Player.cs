﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace programmingGame
{

    public class Player : MonoBehaviour
    {
        [SerializeField]
        private WhipPull whipPull;

        public Vector3 mousePosition;

        [SerializeField]
        private string SecondHat;

        [SerializeField]
        private GameObject narrative;

        [SerializeField]
        private string ThirdHat;

        private bool newHat;

        private float timer;

        private bool neverAgain;

        private bool tooClose;

        public bool TooClose
        {
            get
            {
                return tooClose;
            }
            set
            {
                tooClose = value;
            }
        }

        

        public bool freeMovement = true;

        private Rigidbody2D rb;

        private Vector2 direction;

        [SerializeField]
        private float moveSpeed = 10f;

        private void Start()
        {
            rb = GetComponent<Rigidbody2D>();
        }

        private void OnEnable()
        {
            Message<EventPickUpHats>.Add(SwitchHatsCallback);

            Message<EventStopMoving>.Add(StopMovingCallback);

            Message<EventStartMoving>.Add(StartMovingCallback);

            Message<EventText>.Add(TextCallback);

        }

        private void OnDisable()
        {
            Message<EventPickUpHats>.Remove(SwitchHatsCallback);

            Message<EventStopMoving>.Remove(StopMovingCallback);

            Message<EventStartMoving>.Remove(StartMovingCallback);

            Message<EventText>.Remove(TextCallback);

        }

        private void TextCallback(EventText ev)
        {
            newHat = ev.TextCollider;
        }

        private void SwitchHatsCallback(EventPickUpHats ev)
        {
            whipPull.SecondHat = ev.SecondHat;
            whipPull.ThirdHat = ev.ThirdHat;
        }

        private void StartMovingCallback(EventStartMoving ev)
        {
            freeMovement = ev.Movement;
        }

        private void StopMovingCallback(EventStopMoving ev)
        {
            freeMovement = ev.NoMovement;
        }
        private void OnTriggerEnter2D(Collider2D other)
        {

            if(other.gameObject.tag == SecondHat)
            {
                Message.Raise(new EventPickUpHats(true, false));
                other.gameObject.SetActive(false);

            }

            if (other.gameObject.tag == ThirdHat)
            {
                Message.Raise(new EventPickUpHats(true, true));          
                other.gameObject.SetActive(false);
            }

            if(other.gameObject.tag == "newHat")
            {
                Message.Raise(new EventText(true));
            }
        }

        void hooking()
        {
            if(whipPull.IsHooking == true)
            {
                Message.Raise(new EventStopMoving(false));
            }
        }

        private void notHooking()
        {
            if (whipPull.IsHooking == false)
            {
                Message.Raise(new EventStartMoving(true));
            }
        }

        void faceMouse()
        {
            mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);

            transform.up = direction;

        }
        void FixedUpdate()
        {
            hooking();

            notHooking();

            faceMouse();

            if(newHat == true && neverAgain == false)
            {
                neverAgain = true;
                timer += Time.time;
                narrative.SetActive(true);

                if (timer >= 3)
                {
                    narrative.SetActive(false);
                }
            }

            if (Input.GetMouseButton(0) && freeMovement == true && tooClose == false)
            {
                mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                direction = (mousePosition - transform.position).normalized;
                rb.velocity = new Vector2((direction.x) * Time.deltaTime, (direction.y) * Time.deltaTime).normalized * moveSpeed;
            }
            else
            {
                rb.velocity = Vector2.zero;
            }
        }
    }
}

