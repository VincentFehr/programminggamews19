﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace programmingGame
{
    public class EventPlayerSpawn
    {

        private Transform spawnpoint;
        private GameObject player;

        public Transform Spawnpoint
        {
            get => spawnpoint;
            set => spawnpoint = value;
        }

        public GameObject Player
        {
            get => player;
            set => player = value;
        }

        public EventPlayerSpawn(GameObject Player, Transform spawnpoint)
        {
            Player.transform.position = spawnpoint.position;
        }
    }
}
