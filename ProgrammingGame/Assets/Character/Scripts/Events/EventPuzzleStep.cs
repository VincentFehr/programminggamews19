﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace programmingGame
{
    public class EventPuzzleStep
    {
        public EventPuzzleStep(GameObject ColoredPillar, GameObject AdvancementManagerObj)
        {
            ColoredPillar.GetComponent<CircleCollider2D>().enabled = false;

            AdvancementManagerObj.GetComponent<AdvancementManager>().PuzzleTracker++;
        }
    }
}
