﻿

public class EventHooking 
{
    private bool isHooking;

    public bool IsHooking
    {
        get
        {
            return isHooking;

        }
        set
        {
            isHooking = value;
        }
    }

    public EventHooking(bool isHooking)
    {
        this.isHooking = isHooking;
    }


}
