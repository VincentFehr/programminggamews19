﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventText 
{
    private bool textCollider;

    public bool TextCollider
    {
        get
        {
            return textCollider;
        }
        set
        {
            textCollider = value;
        }       
    }

    public EventText(bool textCollider)
    {
        this.textCollider = textCollider;
    }
}
