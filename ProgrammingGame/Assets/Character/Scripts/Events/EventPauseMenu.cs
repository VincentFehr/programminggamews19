﻿

public class EventPauseMenu
{
    private bool mainMenuButton;

    public bool MainMenuButton
    {
        get
        {
            return mainMenuButton;
        }
        set
        {
            mainMenuButton = value;
        }
    }

    public EventPauseMenu(bool mainMenuButton)
    {
        this.mainMenuButton = mainMenuButton;
    }


}

