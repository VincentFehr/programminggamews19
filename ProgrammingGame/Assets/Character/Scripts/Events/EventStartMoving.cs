﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventStartMoving
{
    private bool movement;

    public bool Movement
    {
        get
        {
            return movement;
        }
        set
        {
            movement = value;
        }
    }

    public EventStartMoving(bool movement)
    {
        this.movement = movement;
    }
}
