﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace programmingGame
{
    public class EventLevelSelect 
    {
        public EventLevelSelect(string SceneName, GameObject StartButton)
        {
            StartButton.GetComponent<StartGame>().Scenename = SceneName;
            Debug.Log("d");
            StartButton.GetComponentInChildren<TextMeshProUGUI>().text = "Start - " + SceneName;
        }
    }
}
