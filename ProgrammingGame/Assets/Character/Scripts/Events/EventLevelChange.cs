﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace programmingGame
{
    public class EventLevelChange
    {
        private string[] Scenes;

        public EventLevelChange( int index)
        {
            SceneManager.LoadScene(index);
        }
    }
}
