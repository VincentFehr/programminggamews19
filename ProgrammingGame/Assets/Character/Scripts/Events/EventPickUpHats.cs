﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace programmingGame
{

    public class EventPickUpHats
    {
        private bool secondHat;


        public bool SecondHat
        { 
           get
           {
                return secondHat;
           }
           set
           {
                secondHat = value;
           }
        }

        private bool thirdHat;

        public bool ThirdHat
        { 
           get
           {
                return thirdHat;
           }
           set
           {
                thirdHat = value;
           }
        }

        public EventPickUpHats(bool secondHat, bool thirdHat)
        {
            this.secondHat = secondHat;

            this.thirdHat = thirdHat;
        }    
    }
}
