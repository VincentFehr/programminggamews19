﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace programmingGame
{
    public class EventPuzzleFinish
    {
        public EventPuzzleFinish(GameObject[] pullPoints)
        {
            for (int i =0; i < pullPoints.Length; i++)
            {
                pullPoints[i].GetComponent<SpriteRenderer>().color = new Color (0, 255, 0, 255);
            }
        }
    }
}