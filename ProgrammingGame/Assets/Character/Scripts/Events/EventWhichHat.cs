﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventWhichHat
{
    private bool hat1;

    public bool Hat1
    {
        get
        {
            return hat1;
        }
        set
        {
            hat1 = value;
        }
    }
    private bool hat2;

    public bool Hat2
    {
        get
        {
            return hat2;
        }
        set
        {
            hat2 = value;
        }
    }

    private bool hat3;

    public bool Hat3
    {
        get
        {
            return hat3;
        }
        set
        {
            hat3 = value;
        }
    }

    public EventWhichHat(bool hat1, bool hat2, bool hat3)
    {
        this.hat1 = hat1;

        this.hat2 = hat2;

        this.hat3 = hat3;
    }
}
