﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventStopMoving
{
    private bool noMovement;

    public bool NoMovement
    {
        get
        {
            return noMovement;
        }
        set
        {
            noMovement = value;
        }
    }

    public EventStopMoving(bool noMovement)
    {
        this.noMovement = noMovement;
    }
}
