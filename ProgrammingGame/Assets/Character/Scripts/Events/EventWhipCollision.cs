﻿
namespace programmingGame
{

    public class EventWhipCollision
    {
        private bool enemyHit;

        public bool EnemyHit
        {
            get { return enemyHit; }
            set { enemyHit = value; }
        }
      
        public EventWhipCollision(bool enemyHit)
        {
            this.enemyHit = enemyHit;
        }

        

       
    }
}