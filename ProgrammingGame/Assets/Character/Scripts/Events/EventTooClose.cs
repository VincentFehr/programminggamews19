﻿

public class EventTooClose
{
    private bool tooClose;

    public bool TooClose
    {
        get
        {
            return tooClose;
        }
        set
        {
            tooClose = value;
        }
    }

    public EventTooClose(bool tooClose)
    {
        this.tooClose = tooClose;
    }
}
