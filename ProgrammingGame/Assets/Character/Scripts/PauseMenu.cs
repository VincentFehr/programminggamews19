﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


namespace programmingGame
{
    public class PauseMenu : MonoBehaviour
    {
        [SerializeField]
        private GameObject pauseMenu;

        [SerializeField]
        private Button[] button;

        private bool mainMenuButton;

        public bool MainMenuButton
        {
            get
            {
                return mainMenuButton;
            }
            set
            {
                mainMenuButton = value;
            }
        }


        private bool escButtonClicked;

        public bool EscButtonClicked
        {
            get
            {
                return escButtonClicked;
            }
            set
            {
                escButtonClicked = value;
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            button[0].onClick.AddListener(resume);
            button[1].onClick.AddListener(mainMenu);
            button[2].onClick.AddListener(quitGame);

        }

        private void OnEnable()
        {
            Message<EventPauseMenu>.Add(PauseMenuCallback);
        }

        private void OnDisable()
        {
            Message<EventPauseMenu>.Remove(PauseMenuCallback);

        }

        private void PauseMenuCallback(EventPauseMenu ev)
        {
            mainMenuButton = ev.MainMenuButton;
        }

        
        public void pause()
        {
            if(Input.GetKeyDown(KeyCode.Escape) == true && EscButtonClicked == false)
            {
                EscButtonClicked = true;
                Debug.Log(EscButtonClicked);
            }

            if (EscButtonClicked == true)
            {
                pauseMenu.gameObject.SetActive(true);
            }

            if (Input.GetKeyDown(KeyCode.Escape) == true && EscButtonClicked==true)
            {
                pauseMenu.gameObject.SetActive(false);
                EscButtonClicked = false;
            }                    
        }


        void resume()
        {
            pauseMenu.gameObject.SetActive(false);
            EscButtonClicked = false;
        }

        void mainMenu()
        {
             SceneManager.LoadScene("MainMenu");         
        }

        void quitGame()
        {
            Application.Quit();
        }
        // Update is called once per frame
        void Update()
        {
            pause();
        }
    }
}
