﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpawnPlayer : MonoBehaviour
{
    [SerializeField]
    private GameObject Player;

    void Start()
    {
        spawnPlayer();
    }

    public void spawnPlayer()
    {
        Player.transform.position = this.transform.position;
    }
}
