﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace programmingGame
{
    public class LevelEnd : MonoBehaviour
    {
        int scenePlusOne;
        string nameOfScene;

        private void Start()
        {
            scenePlusOne = SceneManager.GetActiveScene().buildIndex + 1;

            if (scenePlusOne >= SceneManager.sceneCountInBuildSettings)
            {
                scenePlusOne = 0;
            }
        }

        private void OnEnable()
        {
            Message<EventLevelChange>.Add(PlayerSpawnCallback);
        }

        private void OnDisable()
        {
            Message<EventLevelChange>.Remove(PlayerSpawnCallback);
        }

        private void PlayerSpawnCallback(EventLevelChange ev)
        {
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.CompareTag("Player"))
            {
                Message.Raise(new EventLevelChange(scenePlusOne));
            }
        }
    }
}
