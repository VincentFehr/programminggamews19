﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

namespace programmingGame
{
    public class RestartLevel : MonoBehaviour
    {
        [SerializeField]
        private int health;
        private int currenthealth;

        [SerializeField]
        private TextMeshProUGUI Text;

        private Scene toReloadScene;

        public int Currenthealth
        {
            get => currenthealth;
            set => currenthealth = value;
        }

        private void Start()
        {
            Text = GameObject.Find("Health").GetComponent<TextMeshProUGUI>();

            currenthealth = health;

            toReloadScene = SceneManager.GetActiveScene();
        }

        private void OnEnable()
        {
            Message<EventRestart>.Add(EventRestartCallback);
        }

        private void OnDisable()
        {
            Message<EventRestart>.Remove(EventRestartCallback);
        }

        private void EventRestartCallback(EventRestart ev)
        {
        }

        private void Update()
        {
            Text.text = currenthealth + " / " + health + " Health";

            if (currenthealth == 0)
            {
                Message.Raise(new EventRestart(toReloadScene));
            }
        }
    }
}
