﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace programmingGame
{
    public class MovementPrevention : MonoBehaviour
    {
        [SerializeField]
        private Player player;

        [SerializeField]
        private string thisObject;

        private void Start()
        {
           
        }
        private void OnEnable()
        {
            Message<EventTooClose>.Add(TooCloseCallback);
        }

        private void OnDisable()
        {
            Message<EventTooClose>.Remove(TooCloseCallback);
        }

        private void Update()
        {
            Vector3 mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10);

            mousePos = Camera.main.ScreenToWorldPoint(mousePos);

            mousePos = mousePos - player.transform.position;
            
            if(mousePos.magnitude < 1)
            {
                Message.Raise(new EventTooClose(true));
            }
            else
            {
                Message.Raise(new EventTooClose(false));
            }
        }

        private void TooCloseCallback(EventTooClose ev)
        {
            player.TooClose = ev.TooClose;
        }
    }
}
