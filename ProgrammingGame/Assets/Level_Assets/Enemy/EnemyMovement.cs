﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField]
    private GameObject[] waypoints;

    [SerializeField]
    private GameObject self;

    [SerializeField]
    private Rigidbody2D rb;

    [SerializeField]
    private float travelSpeed;
    [SerializeField]
    private float travelTime;

    private int j = 0;
    bool cooldown = false;

    private void Update()
    {
        transform.position = Vector2.MoveTowards(transform.position, waypoints[j].transform.position, travelSpeed * Time.deltaTime);

        if (Vector2.Distance(transform.position, waypoints[j].transform.position) <= 0.05)
        {
            StartCoroutine(increaseArray());
        }
    }

    IEnumerator increaseArray()
    {
        if (cooldown == true)
        {
            cooldown = false;
        }

        else
        {
            j++;
            cooldown = true;

            if (j >= waypoints.Length)
            {
                j = 0;
            }
        }
        yield return new WaitForSeconds(0.5f);
    }
} 

