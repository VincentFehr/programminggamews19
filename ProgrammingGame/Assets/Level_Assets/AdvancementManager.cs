﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace programmingGame
{
    public class AdvancementManager : MonoBehaviour
    {
        [SerializeField]
        private GameObject AdvancementKey;
        [SerializeField]
        private GameObject[] pullPoints;

        private int puzzleTracker;

        public int PuzzleTracker
        {
            get => puzzleTracker;
            set => puzzleTracker = value;
        }

        private void OnEnable()
        {
            Message<EventPuzzleFinish>.Add(EventPuzzleFinishCallback);
        }

        private void OnDisable()
        {
            Message<EventPuzzleFinish>.Remove(EventPuzzleFinishCallback);
        }

        private void EventPuzzleFinishCallback(EventPuzzleFinish ev)
        {
        }

        private void Start()
        {
            AdvancementKey.SetActive(false);
        }

        private  void Update()
        {
            if (PuzzleTracker == 4 && AdvancementKey.active == false)
            {
                AdvancementKey.SetActive(true);
                Message.Raise(new EventPuzzleFinish(pullPoints));
            }
        }
    }
}
