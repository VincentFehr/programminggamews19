﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace programmingGame
{
    public class PressurePlate : MonoBehaviour
    {
        [SerializeField]
        private GameObject ColoredPillar;

        [SerializeField]
        private GameObject AdvancementManager;

        private void Start()
        {
            AdvancementManager = GameObject.Find("AdvancementManager");
        }

        private void OnEnable()
        {
            Message<EventPuzzleStep>.Add(EventPuzzleStepCallback);
        }

        private void OnDisable()
        {
            Message<EventPuzzleStep>.Remove(EventPuzzleStepCallback);
        }

        private void EventPuzzleStepCallback(EventPuzzleStep ev)
        {
        }


        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject == ColoredPillar)
            {
                Message.Raise(new EventPuzzleStep(ColoredPillar, AdvancementManager));
            }
        }
    }
}
